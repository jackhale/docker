#!/usr/bin/env bash
#
# This script wraps Docker commands to simplify the use of FEniCS Docker images.

# Config
IMAGE_HOST="quay.io/fenicsproject"
DEFAULT_IMAGE="stable"
CACHE_DIR_CONTAINER="/home/fenics/.instant"
PROJECT_DIR_HOST=$(pwd)
PROJECT_DIR_CONTAINER="/home/fenics/shared"
# Workaround for issue https://github.com/docker/docker/issues/9299
DEFAULT_COMMAND="/bin/bash -l -c 'export TERM=xterm; bash -i'"


# Setup
set -e
RED="\033[1;31m"
GREEN="\033[1;32m"
BLUE="\033[1;34m"
NORMAL="\033[0m"

# Detect OS
# https://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux
if [ "$(uname)" == "Darwin" ]; then
    # Mac OS X with Docker installed however you want.
    OS="Darwin"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # If we have Linux, it should work.
    OS="Linux"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    # The Docker environment installed by Docker Toolbox is MinGW based.
    # Docker does not run on 32-bit environments.
    # Don't know if we want to support MSYS or Cygwin as well.
    OS="Windows"
else
    echo -e "${RED}Error${NORMAL}: We do not currently support your operating system $(uname)."
    echo "Contact ${BLUE}fenics-support@googlegroups.com${NORMAL} for assistance."
fi

help ()
{
    echo "Usage: fenicsproject <command> [options]"
    echo ""
    echo "  fenicsproject run [image] [command]   - run a simple FEniCS session"
    echo "  fenicsproject create <name> [image]   - create standard project with given name"
    echo "  fenicsproject notebook <name> [image] - create notebook project with given name"
    echo "  fenicsproject start <name>            - start session in project with given name"
    echo "  fenicsproject pull <image>            - pull latest version of given image"
    echo "  fenicsproject clean-cache [image]     - clean the shared FEniCS (Instant) cache"

    echo ""
    echo "Use 'fenicsproject run' for simple (non-persistent) FEniCS sessions."
    echo ""
    echo "For persistent sessions, use 'create' or 'notebook' followed by 'start'"
    echo "to (re)start a session."
    echo ""
    echo "Available images:"
    echo ""
    echo "  stable  - latest stable release [default]"
    echo "  dev     - latest development version, master branch"
    echo "  dev-env - development environment including dependencies but not FEniCS"
    echo ""
    echo "For more details and tips, see our FEniCS Docker page:"
    echo ""
    echo "  http://fenics-containers.readthedocs.org/en/latest/"
    echo ""
}

command ()
{
    echo "[$@]"
    echo ""
    eval $@
}

command-swallow-stderr ()
{
    echo "[$@]"
    echo ""
    eval $@ 2> /dev/null
}

run ()
{
    IMAGE="$1"
    COMMAND="$2"
    # Setup a container for instant cache
    create-cache-container $IMAGE

    # Run container
    CMD="docker run --rm -ti \
           -v instant-cache-$IMAGE:$CACHE_DIR_CONTAINER \
           -v $PROJECT_DIR_HOST:$PROJECT_DIR_CONTAINER \
           -w $PROJECT_DIR_CONTAINER \
           --label org.fenicsproject.created_by_script=true \
           $IMAGE_HOST/$IMAGE $COMMAND"
    command $CMD
}

create-cache-container ()
{
    IMAGE="$1"
    CMD="docker volume create --name instant-cache-${IMAGE}"
    command $CMD
}

clean-cache ()
{
    IMAGE="$1"
    CMD="docker run --rm -v instant-cache-$IMAGE:$CACHE_DIR_CONTAINER $IMAGE_HOST/$IMAGE 'rm -rf $CACHE_DIR_CONTAINER/*'"
    command $CMD
}

create ()
{
    NAME="$1"
    IMAGE="$2"

    echo -e "Creating new ${GREEN}FEniCS Project${NORMAL} terminal project ${BLUE}$NAME${NORMAL}."
    echo ""

    # Share FENiCS_SRC_DIR if set
    if [ ! -z "$FENICS_SRC_DIR" ]; then
        SHARE_BUILD_DIR="-v $FENICS_SRC_DIR:/home/fenics/build/src"
    fi

    # Create the container
    create-cache-container $IMAGE
    CMD="docker create -ti --name $NAME \
           -v instant-cache-${IMAGE}:$CACHE_DIR_CONTAINER \
           --label org.fenicsproject.created_by_script=true \
           --label org.fenicsproject.project_type='standard' \
           -v $PROJECT_DIR_HOST:$PROJECT_DIR_CONTAINER \
           $SHARE_BUILD_DIR \
           -w $PROJECT_DIR_CONTAINER \
           $IMAGE_HOST/$IMAGE $DEFAULT_COMMAND"
    command $CMD

    # Print summary
    echo ""
    echo -e "To ${BLUE}start the session${NORMAL}, type the following command:"
    echo ""
    echo "  fenicsproject start $NAME"
    echo ""
    echo "You will find the current working directory under ~/shared."
}

start ()
{
    NAME="$1"

    # Determine whether container is already running
    # If not, then start a new session (bash or notebook).
    # If it is, then launch a new bash login terminal.
    IS_RUNNING=$(docker inspect -f '{{.State.Running}}' $NAME 2>/dev/null)
    PROJECT_TYPE=$(docker inspect -f '{{index .Config.Labels "org.fenicsproject.project_type" }}' $NAME 2>/dev/null)
    if [ "$IS_RUNNING" == "false" ]; then
        echo "Starting project named $NAME."
        if [ "$PROJECT_TYPE" == "notebook" ]; then
            # TODO: When Docker shifts over to new Docker for Mac and Docker
            # for Windows, all of this SSH madness should be over.
            if [ "$OS" == "Windows" ]; then
                echo -e "${BLUE}Note${NORMAL}: Notebook support is experimental on Windows."
            fi
            
            CMD="docker start $NAME"
            command $CMD

            # Some SSH tunneling to get the Jupyter session available for people
            # on Mac OS X and Windows (Cygwin). We only expose out to localhost
            # for security.
            PORT=$(docker port $NAME 8888 | cut -d: -f2)
            if [[ "$OS" == "Darwin" || "$OS" == "Windows" ]]; then
                # Ask Docker for the port mapping.
                ACTIVE_MACHINE=$(docker-machine active)
                CMD="docker-machine ssh $ACTIVE_MACHINE -fN -L localhost:$PORT:localhost:$PORT"
                command $CMD
            fi

            echo -e "${BLUE}You can access the Jupyter notebook at http://localhost:${PORT}${NORMAL}"
            # TODO: Broken on Windows.
            # Manually cleanup ssh port forwarding on terminate signal.
            cleanup_ssh () {
                if [[ "$OS" == "Darwin" || "$OS" == "Windows" ]]; then
                    # Find the running ssh PID that created our tunnel and kill it.
                    if [ "$OS" == "Darwin" ]; then
                        PID=$(ps -ax | grep ssh | grep $PORT:localhost:$PORT | awk '{print $1}')
                        kill $PID
                    elif [ "$OS" == "Windows" ]; then
                        # Windows has very limited output from ps. I just kill the ssh
                        # related to the current tty that the script is running in.
                        PID=$(ps -a | grep ssh | grep $(tty | cut -d/ -f3) | awk '{print $1}')
                        kill $PID
                    fi
                fi
            }
            trap cleanup_ssh SIGINT
            CMD="docker attach $NAME"
            command $CMD
        else
           CMD="docker start -ai $NAME"
           command $CMD
       fi
    else
        echo "Starting new session in project named $NAME."
        CMD="docker exec -ti $NAME $DEFAULT_COMMAND"
        command $CMD
    fi
}

pull ()
{
    IMAGE="$1"
    CMD="docker pull $IMAGE_HOST/$IMAGE"
    command $CMD
}

notebook ()
{
    IMAGE="$2"

    NAME="$1"
    echo -e "Creating new ${GREEN}FEniCS Project${NORMAL} notebook project ${BLUE}$NAME${NORMAL}."
    echo ""

    # Setup a container for instant cache
    create-cache-container $IMAGE

    # Run container. We only expose out to localhost for security.
    CMD="docker create -p 127.0.0.1:3000-4000:8888 \
           -v instant-cache-$IMAGE:$CACHE_DIR_CONTAINER \
           -v $PROJECT_DIR_HOST:$PROJECT_DIR_CONTAINER \
           -w $PROJECT_DIR_CONTAINER \
           --label org.fenicsproject.created_by_script=true \
           --label org.fenicsproject.project_type='notebook' \
           --name $NAME \
           $IMAGE_HOST/$IMAGE 'jupyter-notebook --ip=0.0.0.0'"
    command $CMD

    # Print summary
    echo ""
    echo -e "To ${BLUE}start the session${NORMAL}, type the following command:"
    echo ""
    echo "  fenicsproject start $NAME"
    echo ""
}

check_name () {
    NAME=$1
    if [ -z "$NAME" ]; then
        echo -e "${RED}Error${NORMAL}: You must specify the name of the project you want to start."
        exit 1
    fi
    set +e
    IS_RUNNING=$(docker inspect -f '{{.State.Running}}' $NAME 2> /dev/null) 
    if [ $? -eq 1 ]; then
        echo -e "${RED}Error${NORMAL}: Project $NAME does not exist."
        echo "You can create a project with the command:"
        echo ""
        echo "    fenicsproject create $NAME"
        echo ""
        echo "and then try running this command again." 
        exit 1 
    fi
    set -e
}

fail_if_project_already_exists () {
    NAME=$1
    set +e
    IS_RUNNING=$(docker inspect -f '{{.State.Running}}' $NAME 2> /dev/null)
    if [ $? -eq 0 ]; then
        echo -e "${RED}Error${NORMAL}: Project $NAME already exists!"
        echo ""
        echo -e "You can try a different name, or ${BLUE}permanently${NORMAL} delete the existing project"
        echo "with the command:"
        echo ""
        echo "    docker rm $NAME"
        echo ""
        echo -e "Files in the folder ~/shared will ${BLUE}not${NORMAL} be deleted."
        exit 1
    fi
    set -e
}

fail_if_image_does_not_exist () {
    IMAGE=$1
    fail=true
    for image in 'stable' 'dev' 'dev-env' 'dev-env-py3' 'dev-py3' 'dev-env-trilinos' 'dolfin-adjoint' 'dev-env-dbg'; do
        if [ "$IMAGE" == $image ]; then
           fail=false
        fi 
    done
    if [ $fail == true ]; then
        echo -e "${RED}Error${NORMAL}: Image with name $IMAGE does not exist. Try stable, dev-env or dev."
        exit 1
    fi
}

# Check command-line arguments
if [ "$1" == "run" ]; then
    IMAGE="$2" 
    : ${IMAGE:="$DEFAULT_IMAGE"}
    fail_if_image_does_not_exist $IMAGE
    # Select command (if any)
    if [ $# -ge 3 ]; then
        shift; shift;
        COMMAND="$@"
        : ${COMMAND:="$DEFAULT_COMMAND"}
    fi
    run $IMAGE $COMMAND
elif [ "$1" == "create" ]; then
    # Select image
    NAME="$2"
    IMAGE="$3" 
    : ${IMAGE:="$DEFAULT_IMAGE"}
    fail_if_image_does_not_exist $IMAGE
    fail_if_project_already_exists $NAME
    create $NAME $IMAGE
elif [ "$1" == "notebook" ]; then
    NAME="$2"
    IMAGE="$3" 
    : ${IMAGE:="$DEFAULT_IMAGE"}
    fail_if_image_does_not_exist $IMAGE
    fail_if_project_already_exists $NAME
    notebook $NAME $IMAGE
elif [ "$1" == "start" ]; then
    NAME="$2"
    check_name $NAME 
    start $NAME
elif [ "$1" == "pull" ]; then
    IMAGE="$2" 
    : ${IMAGE:="$DEFAULT_IMAGE"}
    fail_if_image_does_not_exist $IMAGE
    pull $IMAGE
elif [ "$1" == "clean-cache" ]; then
    IMAGE="$2" 
    : ${IMAGE:="$DEFAULT_IMAGE"}
    fail_if_image_does_not_exist $IMAGE
    clean-cache $IMAGE
else
    help
    exit 1
fi
